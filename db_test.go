package cshare

import (
	"bytes"
	"encoding/base64"
	"testing"
)

func TestID_Parse(t *testing.T) {
	id := newUniqueID()

	enc := base64.URLEncoding

	s := id.Encode(enc)
	parsed, err := parseID(s, enc)
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(id[:], parsed[:]) {
		t.Fatalf("parseID() failed: orig=%v, parsed=%v", id, parsed)
	}
}
