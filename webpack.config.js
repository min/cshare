const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { SubresourceIntegrityPlugin } = require('webpack-subresource-integrity');

module.exports = {
    mode: 'production',
    context: __dirname + '/ui',
    entry: {
        // Define all files in src/ as sources, everything except js
        // and css will be copied as-is to the static asset directory.
        // This unfortunately prevents automatic data: embedding
        // (see the type: asset/resource directive below).
        cshare: glob.sync(__dirname + '/ui/*.js').concat(
            glob.sync(__dirname + '/ui/*.css')
        ).map(el => {
            return './' + path.basename(el);
        }),
    },
    output: {
        publicPath: '/',
        path: __dirname + '/public',
        filename: '[name].[contenthash].js',
        assetModuleFilename: '[name][ext]',
        crossOriginLoading: 'anonymous',
        clean: true,
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
        }),
        new PurgeCSSPlugin({
            paths: glob.sync(__dirname + '/ui/*.html'),
            safelist: [/^oi/, /^download-/],
        }),
        new SubresourceIntegrityPlugin(),
    ].concat(
        // Process all templates through HtmlWebpackPlugin, to inject
        // SRI-aware links to the webpack-generated top-level assets
        // (CSS and JS).
        glob.sync(__dirname + '/ui/*.html').map(f => new HtmlWebpackPlugin({
            filename: path.basename(f),
            template: f,
            minify: false,
            inject: false,
        }))
    ),
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(ico|png|gif|jpe?g|svg|eot|otf|ttf|woff)$/,
                type: 'asset/resource',
            },
            {
                test: /\.html$/,
                use: [],
            }
        ],
    },
};
