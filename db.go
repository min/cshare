package cshare

import (
	"bytes"
	"context"
	"crypto/rand"
	"database/sql"
	"database/sql/driver"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

func openDB(path string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", path+"?_journal=WAL&_sync=OFF")
	if err != nil {
		return nil, err
	}

	// Limit the pool to a single connection.
	// https://github.com/mattn/go-sqlite3/issues/209
	db.SetMaxOpenConns(1)

	return db, migrate(db)
}

func migrate(db *sql.DB) error {
	tx, err := db.Begin()
	if err != nil {
		return fmt.Errorf("DB migration begin transaction: %w", err)
	}
	defer tx.Rollback() // nolint: errcheck

	var idx int
	if err = tx.QueryRow("PRAGMA user_version").Scan(&idx); err != nil {
		return fmt.Errorf("getting latest applied migration: %w", err)
	}

	if idx == len(migrations) {
		// Already fully migrated, nothing needed.
	} else if idx > len(migrations) {
		return fmt.Errorf("database is at version %d, which is more recent than this binary understands", idx)
	}

	for i, f := range migrations[idx:] {
		if err := f(tx); err != nil {
			return fmt.Errorf("migration to version %d failed: %w", i+1, err)
		}
	}

	// For some reason, ? substitution doesn't work in PRAGMA
	// statements, sqlite reports a parse error.
	if _, err := tx.Exec(fmt.Sprintf("PRAGMA user_version=%d", len(migrations))); err != nil {
		return fmt.Errorf("recording new DB version: %w", err)
	}
	if err := tx.Commit(); err != nil {
		return fmt.Errorf("DB migration commit transaction: %w", err)
	}

	return nil
}

func stmt(idl ...string) func(*sql.Tx) error {
	return func(tx *sql.Tx) error {
		for _, stmt := range idl {
			if _, err := tx.Exec(stmt); err != nil {
				return err
			}
		}
		return nil
	}
}

var migrations = []func(*sql.Tx) error{
	stmt(`
CREATE TABLE files (
    id TEXT NOT NULL PRIMARY KEY,
    expiry DATETIME
)`,
	),
}

// ID is the type for public file identifiers. 20 bytes are enough to
// provide sufficient randomness, and encode to base32 without
// requiring padding, which makes for nicer file names.
type ID [20]byte

func newUniqueID() ID {
	var b [20]byte
	if _, err := io.ReadFull(rand.Reader, b[:]); err != nil {
		panic(err)
	}
	return ID(b)
}

// A common subset of base32.Encoding and base64.Encoding.
type encoding interface {
	EncodeToString([]byte) string
	DecodeString(string) ([]byte, error)
}

func (i ID) Encode(enc encoding) string {
	return enc.EncodeToString(i[:])
}

func (i *ID) parse(s string, enc encoding) error {
	b, err := enc.DecodeString(s)
	if err != nil {
		return err
	}
	if len(b) != len(i) {
		return errors.New("bad length")
	}
	copy(i[:], b[:])
	return nil
}

func parseID(s string, enc encoding) (ID, error) {
	var id ID
	err := id.parse(s, enc)
	return id, err
}

// How IDs are converted to strings in the database.
var dbIDEncoding = base64.URLEncoding

func (i ID) Value() (driver.Value, error) {
	return i.Encode(dbIDEncoding), nil
}

func (i *ID) Scan(value interface{}) error {
	if sv, err := driver.String.ConvertValue(value); err == nil {
		if s, ok := sv.(string); ok {
			return i.parse(s, dbIDEncoding)
		}
	}
	return errors.New("failed to scan ID")
}

func checkFileInDb(ctx context.Context, db *sql.DB, id ID) error {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback() //nolint: errcheck

	var placeholder int
	if err := tx.QueryRow(
		"SELECT 1 FROM files WHERE id = ? AND expiry > ? LIMIT 1", id, time.Now(),
	).Scan(&placeholder); err != nil {
		return err
	}
	return nil
}

// Store a newly uploaded file in the database.
func storeFileInDb(ctx context.Context, db *sql.DB, id ID, expiry time.Time) error {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback() //nolint: errcheck

	if _, err := tx.Exec(
		"INSERT INTO files (id, expiry) VALUES (?, ?)",
		id, expiry,
	); err != nil {
		return err
	}

	return tx.Commit()
}

const cleanupBatchSize = 256

var (
	cleanupTimeout  = 3 * time.Second
	cleanupInterval = 30 * time.Second
)

func (s *Server) expireFiles(ctx context.Context) (bool, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}
	defer tx.Rollback() //nolint: errcheck

	// Delete obsolete files in batches, to avoid locking the
	// database for a long time.
	now := time.Now()
	rows, err := tx.Query("SELECT id FROM files WHERE expiry < ? LIMIT ?", now, cleanupBatchSize)
	if err != nil {
		return false, err
	}
	defer rows.Close()

	// Remove paths as we read them, accumulate ids for later
	// deletion. We build a giant DELETE...WHERE id IN ()
	// statement manually, because database/sql can't handle IN
	// and lists very well. Since we control the format of the
	// IDs, we can get away with simple quoting.
	var queryBuf bytes.Buffer
	var n int
	io.WriteString(&queryBuf, "DELETE FROM files WHERE id IN (") //nolint:errcheck
	for rows.Next() {
		var rid ID
		if err := rows.Scan(&rid); err != nil {
			return false, err
		}
		if n > 0 {
			io.WriteString(&queryBuf, ",") //nolint:errcheck
		}
		n++
		fmt.Fprintf(&queryBuf, "'%s'", rid.Encode(dbIDEncoding))

		if err := s.storage.Delete(rid); err != nil {
			log.Printf("error removing %v: %v", rid, err)
		}
	}
	if err := rows.Err(); err != nil {
		return false, err
	}
	io.WriteString(&queryBuf, ")") //nolint:errcheck

	_, err = tx.Exec(queryBuf.String())
	if err != nil {
		return false, err
	}

	if n > 0 {
		log.Printf("expired %d files", n)
	}

	// Short-circuit the outer loop if we got a full batch (there
	// might be more records to delete right away).
	return (n == cleanupBatchSize), tx.Commit()
}

func (s *Server) cleanup() {
	tick := time.NewTicker(cleanupInterval)
	for range tick.C {
		// Keep deleting stuff in small batches while there is
		// stuff to delete.
		for {
			ctx, cancel := context.WithTimeout(context.Background(), cleanupTimeout)
			cont, err := s.expireFiles(ctx)
			cancel()
			if err != nil {
				log.Printf("cleanup error: %v", err)
				break
			}
			if !cont {
				break
			}
		}
	}
}
