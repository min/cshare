package common

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

// Read default flag values from the environment.
func SetFlagDefaultsFromEnv() (outErr error) {
	flag.VisitAll(func(f *flag.Flag) {
		envvar := "CSHARE_" + strings.ReplaceAll(strings.ToUpper(f.Name), "-", "_")
		if s := os.Getenv(envvar); s != "" {
			if err := f.Value.Set(s); err != nil {
				outErr = fmt.Errorf("invalid value for %s: %w", envvar, err)
			}
		}
	})

	return
}
