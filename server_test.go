package cshare

import (
	"crypto/rand"
	"encoding/json"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

func makeTestRequest(t *testing.T, client *http.Client, method, uri, csrfToken string, body io.Reader, obj interface{}) {
	t.Helper()

	req, err := http.NewRequest(method, uri, body)
	if err != nil {
		t.Fatal(uri, err)
	}
	if csrfToken != "" {
		req.Header.Set("X-CSRF-Token", csrfToken)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(uri, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("%s: HTTP status %d", uri, resp.StatusCode)
	}

	if obj != nil {
		if err := json.NewDecoder(resp.Body).Decode(obj); err != nil {
			t.Fatal(uri, err)
		}
	}
}

func TestServer(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	var b [32]byte
	rand.Read(b[:]) //nolint:errcheck
	srv, err := NewServer(
		filepath.Join(dir, "db"),
		filepath.Join(dir, "storage"),
		b[:],
		300*time.Second,
		50*1024*1024,
		true,
	)
	if err != nil {
		t.Fatal(err)
	}
	defer srv.Close()

	/** To inspect the database on failure:
	defer func() {
		cmd := exec.Command("/bin/sh", "-c", "echo .d | sqlite3 "+filepath.Join(dir, "db"))
		cmd.Stdout = os.Stdout
		cmd.Run()
	}()
	**/

	httpSrv := httptest.NewServer(srv)
	defer httpSrv.Close()

	jar, _ := cookiejar.New(nil)
	client := &http.Client{
		Jar: jar,
	}

	// Request the index page, to bootstrap CSRF.
	makeTestRequest(t, client, "GET", httpSrv.URL, "", nil, nil)

	// Retrieve the CSRF token.
	var csrf struct {
		Token string `json:"token"`
	}
	makeTestRequest(t, client, "GET", httpSrv.URL+"/csrf", "", nil, &csrf)
	if csrf.Token == "" {
		t.Fatal("failed to retrieve a non-empty CSRF token")
	}

	testBody := "test data"
	var uploadResp struct {
		ID string `json:"id"`
	}
	makeTestRequest(t, client, "POST", httpSrv.URL+"/upload", csrf.Token, strings.NewReader(testBody), &uploadResp)
	if uploadResp.ID == "" {
		t.Fatal("failed to retrieve non-empty upload ID")
	}

	// Now test the download, no CSRF required (use a new client
	// that doesn't have access to the CookieJar).
	client2 := new(http.Client)
	makeTestRequest(t, client2, "GET", httpSrv.URL+"/download/"+uploadResp.ID, "", nil, nil)
}
