package main

import (
	"context"
	"crypto/rand"
	"errors"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"git.autistici.org/min/cshare"
	"git.autistici.org/min/cshare/common"
	"github.com/coreos/go-systemd/v22/daemon"
)

var (
	addr        = flag.String("addr", ":3000", "TCP `addr` to listen on")
	dbPath      = flag.String("db", "cshare.db", "database `path`")
	fsRoot      = flag.String("storage", "./storage", "storage `dir`")
	expiry      = flag.Duration("expiry", 12*time.Hour, "expiration time for files")
	maxFileSize = flag.Int("max-file-size", 50, "maximum file size (`MB`)")
	csrfKey     = flag.String("csrf-secret", "", "CSRF secret key (must be exactly 32 bytes)")
	insecure    = flag.Bool("insecure", false, "disable secure bit on CSRF cookies")
)

const MB = 1000 * 1000

func initCSRF() ([]byte, error) {
	var key []byte
	if *csrfKey != "" {
		if len(*csrfKey) != 32 {
			return nil, errors.New("--csrf-secret must be exactly 32 bytes long")
		}
		key = []byte(*csrfKey)
	} else {
		log.Printf("warning: --csrf-secret unset, generating random CSRF secret")
		var b [32]byte
		if _, err := rand.Read(b[:]); err != nil {
			return nil, err
		}
		key = b[:]
	}
	return key, nil
}

func main() {
	log.SetFlags(0)
	if err := common.SetFlagDefaultsFromEnv(); err != nil {
		log.Fatal(err)
	}
	flag.Parse()

	// Initialize the server.
	ckey, err := initCSRF()
	if err != nil {
		log.Fatal(err)
	}
	server, err := cshare.NewServer(*dbPath, *fsRoot, ckey, *expiry, int64(*maxFileSize)*MB, *insecure)
	if err != nil {
		log.Fatal(err)
	}
	defer server.Close()

	// Start the HTTP server, and set it up to terminate
	// gracefully on SIGINT/SIGTERM.
	srv := &http.Server{
		Addr:         *addr,
		Handler:      server,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  600 * time.Second,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		// Gracefully terminate for 10 seconds max, then shut
		// down remaining clients.
		log.Printf("signal received, terminating...")
		daemon.SdNotify(false, "STOPPING=1") // nolint: errcheck
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		if err := srv.Shutdown(ctx); err != nil {
			log.Printf("error stopping HTTP server: %v", err)
		}
		cancel()
		if err := srv.Close(); err != nil {
			log.Printf("error closing HTTP server: %v", err)
		}
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	daemon.SdNotify(false, "READY=1") // nolint: errcheck

	log.Printf("starting HTTP server on %s", *addr)
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("error: %v", err)
	}
}
