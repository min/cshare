package main

import (
	"bytes"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"
	"path"
	"strings"

	"git.autistici.org/min/cshare/common"
	"golang.org/x/text/encoding/unicode"
)

var (
	serverURL = flag.String("server", "", "`URL` of the remote cshare server")
)

func urlJoin(a, b string) string {
	return strings.TrimSuffix(a, "/") + b
}

type header struct {
	MIME string `json:"mime"`
	Name string `json:"name"`
}

func newHeader(name string, data []byte) *header {
	return &header{
		MIME: http.DetectContentType(data),
		Name: name,
	}
}

func (h *header) toUTF16() ([]byte, error) {
	utf8, err := json.Marshal(h)
	if err != nil {
		return nil, err
	}
	encoder := unicode.UTF16(unicode.BigEndian, unicode.IgnoreBOM).NewEncoder()
	return encoder.Bytes(utf8)
}

func buildStream(input io.Reader, name string) ([]byte, error) {
	// Read 512 bytes from the input in order to guess the MIME type.
	tmp := make([]byte, 512)
	n, err := input.Read(tmp)
	if err != nil && errors.Is(err, io.EOF) {
		return nil, err
	}
	tmp = tmp[:n]

	hdr := newHeader(name, tmp)
	serializedHdr, err := hdr.toUTF16()
	if err != nil {
		return nil, err
	}

	buf := new(bytes.Buffer)
	buf.Write(serializedHdr)
	buf.Write([]byte{0, 0}) // UTF-16 NULL
	buf.Write(tmp)
	if _, err = io.Copy(buf, input); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func randomKey() []byte {
	var b [32]byte
	if _, err := io.ReadFull(rand.Reader, b[:]); err != nil {
		panic(err)
	}
	return b[:]
}

func encrypt(key, data []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonce := make([]byte, 12)
	return aesgcm.Seal(nil, nonce, data, nil), nil
}

type csrfResponse struct {
	CSRF string `json:"token"`
}

func getCSRF(ctx context.Context, client *http.Client) (string, error) {
	req, err := http.NewRequest("GET", urlJoin(*serverURL, "/csrf"), nil)
	if err != nil {
		return "", err
	}
	resp, err := client.Do(req.WithContext(ctx))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
	var csrf csrfResponse
	if err := json.NewDecoder(resp.Body).Decode(&csrf); err != nil {
		return "", err
	}
	return csrf.CSRF, nil
}

type uploadResponse struct {
	ID string `json:"id"`
}

func upload(ctx context.Context, encData []byte) (string, error) {
	jar, err := cookiejar.New(nil)
	if err != nil {
		return "", err
	}
	client := &http.Client{
		Jar: jar,
	}

	// Acquire the CSRF token.
	csrf, err := getCSRF(ctx, client)
	if err != nil {
		return "", fmt.Errorf("error fetching CSRF token: %w", err)
	}

	// Make the request.
	req, err := http.NewRequest("POST", urlJoin(*serverURL, "/upload"), bytes.NewReader(encData))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/octet-stream")
	req.Header.Set("X-CSRF-Token", csrf)
	resp, err := client.Do(req.WithContext(ctx))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
	var uresp uploadResponse
	if err := json.NewDecoder(resp.Body).Decode(&uresp); err != nil {
		return "", err
	}
	return uresp.ID, nil
}

func encodeToken(key []byte, id string) (string, error) {
	bid, err := base64.URLEncoding.DecodeString(id)
	if err != nil {
		return "", err
	}
	key = append(key, bid...)
	return base64.URLEncoding.EncodeToString(key), nil
}

func main() {
	log.SetFlags(0)
	if err := common.SetFlagDefaultsFromEnv(); err != nil {
		log.Fatal(err)
	}
	flag.Parse()

	if *serverURL == "" {
		log.Fatal("Must specify --server")
	}

	var filename string
	var input io.Reader
	if flag.NArg() > 0 {
		filename = path.Base(flag.Arg(0))
		f, err := os.Open(flag.Arg(0))
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		input = f
	} else {
		filename = "stdin"
		input = os.Stdin
	}

	data, err := buildStream(input, filename)
	if err != nil {
		log.Fatal(err)
	}
	key := randomKey()
	enc, err := encrypt(key, data)
	if err != nil {
		log.Fatal(err)
	}

	id, err := upload(context.Background(), enc)
	if err != nil {
		log.Fatal(err)
	}

	token, err := encodeToken(key, id)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s/#/dl/%s\n", *serverURL, token)
}
