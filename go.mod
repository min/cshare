module git.autistici.org/min/cshare

go 1.19

require (
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/dustin/go-humanize v1.0.1
	github.com/gorilla/csrf v1.7.2
	github.com/gorilla/handlers v1.5.2
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/vearutop/statigz v1.4.0
	golang.org/x/text v0.14.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/gorilla/securecookie v1.1.2 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
)
