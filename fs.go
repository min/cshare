package cshare

import (
	"bufio"
	"bytes"
	"encoding/base32"
	"io"
	"io/fs"
	"os"
	"path/filepath"
)

// bufio.Writer wrapper that flushes on Close.
type bufferedWriter struct {
	io.Closer
	*bufio.Writer
}

func newBufferedWriter(w io.WriteCloser) *bufferedWriter {
	return &bufferedWriter{
		Closer: w,
		Writer: bufio.NewWriter(w),
	}
}

func (w *bufferedWriter) Close() error {
	err := w.Writer.Flush()
	if cerr := w.Closer.Close(); cerr != nil && err == nil {
		err = cerr
	}
	return err
}

type storage interface {
	Open(ID) (fs.File, error)
	Create(ID) (io.WriteCloser, error)
	Delete(ID) error
}

var fsIDEncoding = base32.StdEncoding

type localStorage struct {
	root  string
	depth int
}

func newLocalStorage(path string) *localStorage {
	return &localStorage{
		root:  path,
		depth: 2,
	}
}

func (s *localStorage) path(id ID) string {
	return filepath.Join(s.root, shardedPath(id.Encode(fsIDEncoding), s.depth))
}

func (s *localStorage) Open(id ID) (fs.File, error) {
	return os.Open(s.path(id))
}

func (s *localStorage) Create(id ID) (io.WriteCloser, error) {
	path := s.path(id)

	// Ensure the containing directory exists.
	if dir := filepath.Dir(path); dir != "." {
		if err := os.MkdirAll(dir, 0700); err != nil {
			return nil, err
		}
	}

	// Create the new file.
	f, err := os.Create(path)
	if err != nil {
		return nil, err
	}

	return newBufferedWriter(f), nil
}

func (s *localStorage) Delete(id ID) error {
	return os.Remove(s.path(id))
}

// Function that determines the layout of the storage filesystem tree.
// It creates a multi-level hierarchy, adaptable for sites with high
// traffic.
func shardedPath(filename string, levels int) string {
	if len(filename) < levels {
		return filename
	}

	var buf bytes.Buffer
	for i := 0; i < levels; i++ {
		buf.WriteByte(filename[i]) //nolint:errcheck
		buf.WriteByte('/')         //nolint:errcheck
	}
	buf.WriteString(filename) //nolint:errcheck

	return buf.String()
}
