import { b64encode, b64decode } from './base64.js';
import { decrypt } from './crypto.js';

// *** UPLOAD ***

function encodeToken(key, id) {
    var b = new Uint8Array(key.length + id.length);
    for (let i = 0; i < key.length; i++) {
        b[i] = key[i];
    }
    for (let i = 0; i < id.length; i++) {
        b[key.length + i] = id[i];
    }
    return b64encode(b);
}

function withCSRF() {
    console.log("fetching CSRF token");
    return fetch("/csrf", {})
        .then(response => {
            if (!response.ok) {
                throw new Error("HTTP status " + response.status);
            }
            return response.json();
        });
}

// Upload data bundle, encrypted with key.
export function upload(data, key) {
    return withCSRF().then(csrf => {
        return fetch("/upload", {
            method: 'POST',
            body: data,
            headers: {
                'Content-Type': 'application/octet-stream',
                'X-CSRF-Token': csrf.token,
            },
        })
    })
        .then(response => {
            if (!response.ok) {
                throw new Error("HTTP status " + response.status);
            }
            return response.json();
        })
        .then(result => {
            var id = b64decode(result.id);
            var token = encodeToken(key, id);
            var dlurl = window.location.origin + window.location.pathname + "#/dl/" + token;
            return dlurl;
        });
}

// Convert a string to an ArrayBuffer containing its UTF-16 representation.
function str2ab(str) {
    var buf = new ArrayBuffer(str.length * 2);
    var bufView = new DataView(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView.setUint16(i * 2, str.charCodeAt(i), false)
    }
    return buf;
}

// Convert a string to an Uint8Array.
function str2ua(str) {
    const ua = new Uint8Array(str.length);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        ua[i] = str.charCodeAt(i);
    }
    return ua;
}

// Serialize the given file, return a Promise so we can use the result
// asynchronously.
function serialize(fileObj, name, mimeType) {
    var hdr = str2ab(JSON.stringify({
        name: name,
        mime: mimeType,
    }));
    var zero = new Uint8Array([0, 0]);
    var blob = new Blob([hdr, zero, fileObj]);
    var fr = new FileReader();

    return new Promise((resolve, reject) => {
        fr.onerror = function() {
            fr.abort();
            reject(fr.error);
        };
        fr.onload = function(evt) {
            resolve(evt.target.result);
        };
        fr.readAsArrayBuffer(blob);
    });
}

// Serialize metadata header and file data to a buffer, and call fn
// with the result.
export function serializeFile(file, name) {
    return serialize(file, name, file.type);
}

// Serialize metadata header and file data to a buffer, and call fn
// with the result.
export function serializePaste(pastedData) {
    return serialize(str2ua(pastedData), 'paste', 'text/plain');
}

// *** DOWNLOAD ***

function decodeToken(token) {
    var decoded = b64decode(token);
    if (decoded.length < 33) {
        throw new Error('bad token length');
    }
    return {
        key: decoded.slice(0, 32),
        id: decoded.slice(32),
    };
}

// Decode metadata header and file data from an ArrayBuffer.
function deserialize(data) {
    var header = '';
    var headerview = new DataView(data);
    var i = 0;
    // The header should be a NULL-terminated UTF-16 string. But don't read too far anyway.
    for (; i < (data.byteLength/2); i++) {
        var num = headerview.getUint16(i * 2, false)
        if (num == 0) {
            break;
        }
        header += String.fromCharCode(num);
    }
    var header = JSON.parse(header);
    var filedata = data.slice((i * 2) + 2, data.byteLength);
    return {
        header: header,
        data: filedata,
    };
}

var staticheader = [123, 125, 0, 0];

function stripHeader(data) {
    if (data.byteLength < 4) {
        return data;
    }
    var uarr = new Uint8Array(data.slice(0, staticheader.length));
    for (var i = 0; i < staticheader.length; i++) {
        if (staticheader[i] != uarr[i]) {
            return data;
        }
    }
    return data.slice(staticheader.length, data.byteLength);
}

// Return a safe MIME type for content consumption. We basically just
// nuke things that have special meaning for the browser.
function safeMime(mime) {
    if (mime.startsWith("text/html")
        || mime.startsWith("text/css")
        || mime.startsWith("application/javascript")
        || mime.startsWith("application/x-javascript")) {
        return "text/plain";
    }
    return mime;
}

export function download(encToken) {
    var token = decodeToken(encToken);
    var apiUrl = '/download/' + b64encode(token.id);
    return fetch(apiUrl, {})
        .then(response => {
            if (!response.ok) {
                throw new Error("HTTP status " + response.status);
            }
            return response.arrayBuffer();
        })
        .then(data => {
            return decrypt(token.key, stripHeader(data));
        })
        .then(decodedData => {
            var result = deserialize(decodedData);
            console.log(result.header);

            // Store the file data in a Blob and create a URL for it.
            var mime = safeMime(result.header.mime);
            var blob = new Blob([result.data], {type: mime});
            var url = URL.createObjectURL(blob);
            return {
                url: url,
                mime: mime,
                name: result.header.name,
            };
        });
}
