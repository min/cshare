var crypto = window.crypto || window.msCrypto;

const algoName = "AES-GCM";
const algoParams = {
    name: algoName,
    length: 256,
};

// Return n random bytes as a Uint8Array.
/**
export function getRandomBytes(n) {
    let bytes = '';
    const byteArray = new Uint8Array(length);
    crypto.getRandomValues(byteArray);
    for (let i = 0; i < length; ++i) {
        bytes += String.fromCharCode(byteArray[i]);
    }
    return bytes;
}
**/

export async function encrypt(data) {
    const key = await crypto.subtle.generateKey(algoParams, true, ["encrypt"]);
    const rawkey = await crypto.subtle.exportKey("raw", key);

    // Use a NULL IV because encryption keys are never re-used.
    const iv = new Uint8Array(12);

    return {
        rawkey: new Uint8Array(rawkey),
        encrypted: await crypto.subtle.encrypt({
            name: algoName,
            iv: iv,
        }, key, data),
    }
}

export async function decrypt(rawkey, data) {
    // Convert the rawkey from Uint8Array to an ArrayBuffer.
    const rawkeybuf = rawkey.buffer.slice(rawkey.byteOffset, rawkey.byteLength + rawkey.byteOffset)

    const key = await crypto.subtle.importKey(
        "raw",
        rawkeybuf,
        algoName,
        false,
        ["decrypt"],
    );

    const iv = new Uint8Array(12);
    return crypto.subtle.decrypt({
        name: algoName,
        iv: iv,
    }, key, data);
}
