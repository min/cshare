import { b64encode, b64decode } from './base64.js';
import { encrypt } from './crypto.js';
import { serializeFile, serializePaste, upload, download } from './transfer.js';

function showElement(el) {
    el.classList.remove('hidden');
}

function hideElement(el) {
    el.classList.add('hidden');
}

function showError(msg) {
    console.log('ERROR: ' + msg);

    var p = document.createElement('p');
    p.appendChild(document.createTextNode('ERROR: ' + msg));
    p.classList.add('error');

    var status = document.getElementById('statusDiv');
    status.innerHTML = '';
    status.appendChild(p);

    showDiv('statusDiv');
}

function showDiv(divName) {
    document.querySelectorAll('.content-tab').forEach(el => {
        hideElement(el);
    });
    showElement(document.getElementById(divName));
}

function uploadOk(url) {
    var a = document.getElementById('uploadDownloadLink');
    a.href = url;
    a.text = url;
    redirectTo('/upload-ok');
}

function doUpload(file) {
    showDiv('uploadingDiv');

    console.log('uploading file "' + file.name + '", ' + file.size + ' bytes');

    serializeFile(file, file.name)
        .then((data) => {
            return encrypt(data);
        })
        .then((res) => {
            return upload(res.encrypted, res.rawkey);
        })
        .then((dlurl) => {
            uploadOk(dlurl);
        })
        .catch(e => { showError("Upload failed: " + e.message); });
}

function doUploadPaste(pastedData) {
    showDiv('uploadingDiv');

    console.log('uploading pasted data');

    serializePaste(pastedData)
        .then((data) => {
            return encrypt(data);
        })
        .then((res) => {
            return upload(res.encrypted, res.rawkey);
        })
        .then((dlurl) => {
            uploadOk(dlurl);
        }).catch(e => { showError("Upload failed: " + e.message); });
}

// Can we just display this MIME type in the browser?
function isMimeViewable(mime) {
    if (mime.startsWith("text/")
        || mime.startsWith("image/")
        || mime.startsWith("video/")
        || mime.startsWith("audio/")
        || mime.startsWith("application/pdf")) {
        return true;
    }
    return false;
}

function doDownload(encToken) {
    showDiv('downloadingDiv');

    console.log('starting download');

    download(encToken)
        .then(result => {

            console.log("downloading " + result.url + ", mime=" + result.mime + ", name=" + result.name);

            // Some MIME types are directly viewable, for others we create a download button.
            if (isMimeViewable(result.mime)) {
                showDiv('readyDiv');
                window.location.href = result.url;
            } else {
                console.log("created download link");
                var a = document.getElementById('downloadLink');
                a.href = result.url;
                a.download = result.name;
                showDiv('downloadDiv');
            }
        })
        .catch(e => { showError("Download failed: " + e.message); });
}

function redirectTo(uri) {
    console.log('redirecting to ' + uri);
    window.location.hash = '#' + uri;
}

// Prevent showing upload-ok if not after upload.
var afterUpload = false;

const routes = {
    '404': function() {
        redirectTo('/upload');
    },
    '/': function() {
        redirectTo('/upload');
    },
    'upload': function() {
        showDiv('uploadDiv');
        afterUpload = true;

        // Update information about runtime parameters.
        fetch('/params')
            .then(response => {
                if (!response.ok) {
                    throw new Error('HTTP status ' + response.status);
                }
                return response.json();
            })
            .then(result => {
                document.getElementById('maxFileSizeSpan').innerText = result.max_file_size_human;
                document.getElementById('expirySpan').innerText = result.expiry_human;
            });
    },
    'upload-ok': function() {
        if (afterUpload) {
            showDiv('uploadOkDiv');
            afterUpload = false;
        } else {
            redirectTo('/upload');
        }
    },
    'dl': function(encToken) {
        doDownload(encToken);
    },
};

const locationHandler = function() {
    // get the url path
    var location = window.location.hash.replace(/^#\//, ''), arg;
    // if the path length is 0, set it to primary page route
    if (location.length == 0) {
        location = "/";
    } else {
        var n = location.indexOf('/');
        if (n > 0) {
            arg = location.substr(n+1);
            location = location.substr(0, n);
        }
    }

    // get the route object from the routes object and run it
    console.log('route: ' + location);
    const route = routes[location] || routes["404"];
    route(arg);
};

window.addEventListener("load", () => {
    var uploadField = document.getElementById("uploadField");
    uploadField.addEventListener("change", function(e) {
        e.preventDefault();
        if (uploadField.files.length > 0) {
            console.log("found a file to upload!");
            doUpload(uploadField.files[0]);
        }
        return false;
    }, false);

    document.addEventListener("paste", (event) => {
        event.preventDefault();
        let paste = (event.clipboardData || window.clipboardData).getData("text");
        doUploadPaste(paste);
    });

    window.addEventListener("hashchange", locationHandler);
    locationHandler();
});
