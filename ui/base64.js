// Use a lookup table to find the index.
const b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
const b64lookup = new Uint8Array(256);
for (let i = 0; i < b64chars.length; i++) {
    b64lookup[b64chars.charCodeAt(i)] = i;
}

export function b64encode(bytes) {
    let i,
        len = bytes.length,
        base64 = '';

    for (i = 0; i < len; i += 3) {
        base64 += b64chars[bytes[i] >> 2];
        base64 += b64chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
        base64 += b64chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
        base64 += b64chars[bytes[i + 2] & 63];
    }

    if (len % 3 === 2) {
        base64 = base64.substring(0, base64.length - 1) + '=';
    } else if (len % 3 === 1) {
        base64 = base64.substring(0, base64.length - 2) + '==';
    }

    return base64;
}

export function b64decode(base64) {
    let bufferLength = base64.length * 0.75,
        len = base64.length,
        i,
        p = 0,
        encoded1,
        encoded2,
        encoded3,
        encoded4;

    if (base64[len - 1] === '=') {
        bufferLength--;
        if (base64[len - 2] === '=') {
            bufferLength--;
        }
    }

    const bytes = new Uint8Array(bufferLength);

    for (i = 0; i < len; i += 4) {
        encoded1 = b64lookup[base64.charCodeAt(i)];
        encoded2 = b64lookup[base64.charCodeAt(i + 1)];
        encoded3 = b64lookup[base64.charCodeAt(i + 2)];
        encoded4 = b64lookup[base64.charCodeAt(i + 3)];

        bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
        bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
        bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
    }

    return bytes;
}
