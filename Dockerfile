FROM docker.io/library/golang:1.22.1 as build
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install npm brotli
ADD . /src
RUN cd /src && \
    go generate . && \
    go build -ldflags="-extldflags=-static" -tags "sqlite_omit_load_extension netgo" -o cshare ./cmd/cshare && \
    strip cshare

FROM scratch
COPY --from=build /src/cshare /cshare

ENTRYPOINT ["/cshare"]

