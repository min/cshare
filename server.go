package cshare

//go:generate npm install
//go:generate ./node_modules/.bin/webpack
//go:generate find public -type f -regextype egrep -regex .*\.(css|js|svg|html) -exec brotli -9kv {} +
//go:generate find public -type f -regextype egrep -regex .*\.(css|js|svg|html) -exec gzip -9kv {} +

import (
	"database/sql"
	"embed"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/gorilla/csrf"
	"github.com/gorilla/handlers"
	"github.com/vearutop/statigz"
	"github.com/vearutop/statigz/brotli"
)

var requestTimeout = 60 * time.Second

//go:embed public
var assetsFS embed.FS

// ID encoding used by the public API. This should match the JS code.
var apiIDEncoding = base64.URLEncoding

// http.ResponseWriter that adds a cache-friendly header on responses
// with a successful (2xx) status.
type cacheResponseWriter struct {
	http.ResponseWriter
}

func newCacheResponseWriter(w http.ResponseWriter) *cacheResponseWriter {
	return &cacheResponseWriter{
		ResponseWriter: w,
	}
}

func (w *cacheResponseWriter) WriteHeader(statusCode int) {
	if statusCode >= 200 && statusCode < 300 {
		hdr := w.ResponseWriter.Header()
		if ct := hdr.Get("Content-Type"); !strings.HasPrefix(ct, "text/html") && !strings.HasPrefix(ct, "application/json") {
			hdr.Set("Cache-Control", fullCache)
		}
	}
	w.ResponseWriter.WriteHeader(statusCode)
}

const fullCache = "max-age=2592000"

// Wrap an http.Handler with cache-friendly headers.
func cacheHeaders(wrap http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		wrap.ServeHTTP(newCacheResponseWriter(w), req)
	})
}

const cspValue = "default-src 'self' image-src blob:"

func contentSecurityPolicy(wrap http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Security-Policy", cspValue)
		wrap.ServeHTTP(w, req)
	})
}

type Server struct {
	http.Handler

	db          *sql.DB
	storage     storage
	expiry      time.Duration
	maxFileSize int64
}

func NewServer(dbpath, fsRoot string, csrfKey []byte, expiry time.Duration, maxFileSize int64, insecureCSRF bool) (*Server, error) {
	db, err := openDB(dbpath)
	if err != nil {
		return nil, err
	}

	s := &Server{
		db:          db,
		storage:     newLocalStorage(fsRoot),
		expiry:      expiry,
		maxFileSize: maxFileSize,
	}

	go s.cleanup()

	// Protected endpoints (behind CSRF). The slightly verbose
	// routing setup is due to the requirement of going through a
	// single middleware instance.
	pmux := http.NewServeMux()
	pmux.HandleFunc("/csrf", s.handleCSRFRequest)
	pmux.Handle("/upload", http.MaxBytesHandler(
		http.HandlerFunc(s.handleUpload), maxFileSize))
	protected := csrf.Protect(
		csrfKey,
		csrf.Secure(!insecureCSRF),
		csrf.CookieName("_csrf"),
	)(pmux)

	mux := http.NewServeMux()
	mux.Handle("/csrf", protected)
	mux.Handle("/upload", protected)
	mux.Handle("/download/", http.StripPrefix("/download/", http.HandlerFunc(s.handleDownload)))
	mux.Handle("/params", cacheHeaders(http.HandlerFunc(s.handleParams)))

	// The root (fallback) handler serves our static content and
	// adds custom cache-friendly headers.
	mux.Handle("/", contentSecurityPolicy(
		cacheHeaders(
			statigz.FileServer(assetsFS,
				brotli.AddEncoding,
				statigz.FSPrefix("public")))))

	// Add global middleware.
	s.Handler = handlers.CombinedLoggingHandler(
		os.Stdout,
		http.TimeoutHandler(mux, requestTimeout, ""))

	return s, nil
}

func (s *Server) Close() {
	s.db.Close()
}

func (s *Server) handleParams(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(struct {
		MaxFileSize string `json:"max_file_size_human"`
		Expiry      string `json:"expiry_human"`
	}{
		MaxFileSize: humanize.Bytes(uint64(s.maxFileSize)),
		Expiry:      humanizeDuration(s.expiry),
	})
}

func (s *Server) handleCSRFRequest(w http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store")
	// nolint: errcheck
	json.NewEncoder(w).Encode(struct {
		Token string `json:"token"`
	}{
		Token: csrf.Token(req),
	})
}

func (s *Server) handleUpload(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	id := newUniqueID()
	expiry := time.Now().Add(s.expiry)

	f, err := s.storage.Create(id)
	if err != nil {
		log.Printf("error creating upload file: %v", err)
		http.Error(w, "Oops", http.StatusInternalServerError)
		return
	}
	defer f.Close()

	_, err = io.Copy(f, req.Body)
	if err != nil {
		log.Printf("error receiving file: %v", err)
		// Can't use errors.Is because net/http creates a new
		// instance of MaxBytesError every time.
		if _, ok := err.(*http.MaxBytesError); ok {
			http.Error(w, "Request too large", http.StatusRequestEntityTooLarge)
		} else {
			http.Error(w, "Oops", http.StatusInternalServerError)
		}
		return
	}

	if err := storeFileInDb(req.Context(), s.db, id, expiry); err != nil {
		// Remove the file! It's not going to be in the db.
		s.storage.Delete(id) //nolint:errcheck
		log.Printf("error finalizing upload: %v", err)
		http.Error(w, "Oops", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-store")

	// nolint: errcheck
	json.NewEncoder(w).Encode(struct {
		ID string `json:"id"`
	}{
		ID: ID.Encode(id, apiIDEncoding),
	})
}

// Little header to discourage direct linking.
var staticHeader = []byte{123, 125, 0, 0}

func (s *Server) handleDownload(w http.ResponseWriter, req *http.Request) {
	id, err := parseID(req.URL.Path, apiIDEncoding)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	if err := checkFileInDb(req.Context(), s.db, id); err != nil {
		log.Printf("download: %v", err)
		http.NotFound(w, req)
		return
	}

	f, err := s.storage.Open(id)
	if err != nil {
		log.Printf("warning: file exists in db but not on the fs: %v", id)
		http.NotFound(w, req)
		return
	}
	defer f.Close()

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Cache-Control", "no-store")
	w.Write(staticHeader) // nolint: errcheck
	io.Copy(w, f)         // nolint: errcheck
}

// Format a time.Duration without annoying '0' suffixes.
func humanizeDuration(d time.Duration) string {
	s := d.String()
	if strings.HasSuffix(s, "m0s") {
		s = s[:len(s)-2]
	}
	if strings.HasSuffix(s, "h0m") {
		s = s[:len(s)-2]
	}
	return s
}
